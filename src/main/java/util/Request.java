package util;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Properties;

import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.auth.FirebaseCredentials;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import model.Review;

public class Request {
	public void postData(List<Review> reviews, String endPoint) throws IOException {
		InputStream input = null;
		final Properties properties = new Properties();
		properties.load(Request.class.getResourceAsStream("/application.properties"));
		try {
			input = Request.class.getResourceAsStream("/service_account.json");
		} catch (Exception e) {
			e.printStackTrace();
		}

		FirebaseOptions options = new FirebaseOptions.Builder()
				.setCredential(FirebaseCredentials.fromCertificate(input))
				.setDatabaseUrl(properties.getProperty("firebase.base.url")).build();
		if (!(FirebaseApp.getApps().size() > 0)) {
			FirebaseApp.initializeApp(options);
		}

		final FirebaseDatabase database = FirebaseDatabase.getInstance();
		DatabaseReference dbref = database.getReference("review");
		DatabaseReference usersRef = dbref.child(endPoint);
		usersRef.setValue(reviews);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (FirebaseDatabase.getInstance() != null) {
			System.out.println("i m done " + endPoint);
//			Firebase.goOffline();
		}
	}
}
