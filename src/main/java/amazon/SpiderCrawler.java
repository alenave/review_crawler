package amazon;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import model.Review;
import util.Request;

import org.jsoup.nodes.Element;

public class SpiderCrawler {

	private Set<String> pagesVisited = new HashSet<String>();
	private List<String> pagesToVisit = new LinkedList<String>();
	private List<Review> reviewList = new ArrayList<>();

	private static final String USER_AGENT = "Mozilla/5.0 (Windows NT 6.3; rv:36.0) Gecko/20100101 Firefox/36.0 AppleWebKit/535.1 (KHTML, like Gecko) Chrome/13.0.782.112 Safari/535.1 Opera/9.80";
	final Properties properties = new Properties();

	public void connections(String url) throws IOException {
		properties.load(SpiderLeg.class.getResourceAsStream("/application.properties"));
		Connection.Response response;
		response = Jsoup.connect(url).userAgent(USER_AGENT).timeout(3000).execute();
		System.out.println(properties.getProperty("amazon") + url);
		if (response.statusCode() == 200) {
			Document htmlDocument = response.parse();
			Review amazon = new Review();
			Elements reviews = htmlDocument.getElementsByAttributeValue("data-hook", "review");
			for (Element review : reviews) {
				Elements elements = review.getElementsByAttributeValue("data-hook", "review-star-rating");
				for (Element ele : elements) {
					amazon.setStarRating(ele.getElementsByTag("span").text());
				}
				elements = review.getElementsByAttributeValue("data-hook", "review-title");
				for (Element ele : elements) {
					amazon.setTitle(ele.text());
				}
				elements = review.getElementsByAttributeValue("data-hook", "review-body");
				for (Element ele : elements) {
					amazon.setBody(ele.text());
				}
			}
			reviewList.add(amazon);
		}
	}

	public void crawler(List<String> list) throws IOException {
		this.pagesToVisit.addAll(list);
		while (this.pagesToVisit.size() != 0) {
			String currentUrl = null;
			if (this.pagesToVisit.isEmpty()) {
				currentUrl = list.get(0);
				connections(currentUrl);
				this.pagesVisited.add(currentUrl);
			} else {
				currentUrl = this.nextUrl();
				connections(currentUrl);
			}
			System.out.println(this.pagesVisited.size() + " " + this.pagesToVisit.size());
		}
		new Request().postData(reviewList, properties.getProperty("firebase.amazon"));
	}

	String nextUrl() {
		String nextUrl;
		do {
			nextUrl = this.pagesToVisit.remove(0);

		} while (this.pagesVisited.contains(nextUrl));
		this.pagesVisited.add(nextUrl);
		return nextUrl;
	}

}
