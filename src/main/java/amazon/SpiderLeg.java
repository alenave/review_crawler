package amazon;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class SpiderLeg {
	private static final String USER_AGENT = "Mozilla/5.0 (Windows NT 6.3; rv:36.0) Gecko/20100101 Firefox/36.0 AppleWebKit/535.1 (KHTML, like Gecko) Chrome/13.0.782.112 Safari/535.1 Opera/9.80";
	private List<String> links = new ArrayList<String>();

	public int crawl(int pageNumber) throws IOException {
		final Properties properties = new Properties();
		properties.load(SpiderLeg.class.getResourceAsStream("/application.properties"));

		int num = -1;
		try {

			for (int pageIndex = 1; pageIndex <= pageNumber; pageIndex++) {
				String page = "&page=" + pageIndex;
				Document htmlDocument;
				Connection.Response response;
				response = Jsoup
						.connect(properties.getProperty("amazon") + properties.getProperty("amazon.mobile") + page)
						.userAgent(USER_AGENT).timeout(3000).execute();
				System.out.println(properties.getProperty("amazon.mobile") + page);
				if (response.statusCode() == 200) {
					htmlDocument = response.parse();
					Elements urlElement = htmlDocument.getElementsByAttributeValue("class", "s-result-item  celwidget ");
					for (Element url : urlElement) {
						Elements hrefs = url.getElementsByAttributeValue("class",
								"a-size-small a-link-normal a-text-normal");
						for (Element href : hrefs) {
							String reviewUrl = href.getElementsByTag("a").attr("href");
							if (reviewUrl.contains("customerReviews"))
								setLinks(href.getElementsByTag("a").attr("href"));
						}
					}
				}
			}
			new SpiderCrawler().crawler(getLinks());
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e);
		}

		return num;
	}

	public List<String> getLinks() {

		return links;
	}

	public void setLinks(String href) {
		links.add(href);
	}

}
