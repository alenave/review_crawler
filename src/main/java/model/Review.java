package model;

public class Review {
	String starRating;
	String title;
	String body;

	public String getBody() {
		return body;
	}

	public String getStarRating() {
		return starRating;
	}

	public String getTitle() {
		return title;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public void setStarRating(String starRating) {
		this.starRating = starRating;
	}

	public void setTitle(String title) {
		this.title = title;
	}
}
