package crawler;

import java.io.IOException;
import java.util.Properties;

import amazon.SpiderLeg;

public class Application {

	public static void main(String args[]) throws IOException {
		final Properties properties = new Properties();
		properties.load(Application.class.getResourceAsStream("/application.properties"));
		new SpiderLeg().crawl(4);
	}
}